(ns chime-test.core
  (:gen-class)
   (:require
            [chime :refer [chime-ch]]
            [clj-time.periodic :refer [periodic-seq]]
            [clj-time.core :as t]
            [clojure.core.async :as a :refer [<! go-loop]]
            ))

(def times (rest
  (periodic-seq (t/now)
               (-> 5 t/seconds))))

(def chime(chime-ch times
                    {:ch(a/chan (a/sliding-buffer 1))}))

(defn startloop [chime]
  (go-loop []
    (when-let [time (<! chime)]
     (println "Hello")
      (recur))))

(defn stoploop [chime]
  (a/close! chime)
  )



(defn -main [& args]
  (startloop chime))
